/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package erosion;

import ages.types.HRU;
import ages.types.Landuse;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import crop.Crop;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.time.LocalDate;

@Description("Add Musle module definition here")
@Author(name = "Holm Kipka, Olaf David, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Erosion")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/erosion/Musle.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/erosion/Musle.xml")
public class MusleAdapter extends AnnotatedAdapter {
    @Description("daily or hourly time steps [d|h]")
    @Units("d | h")
    @Role(PARAMETER)
    @Input public String tempRes;

    @Role(PARAMETER)
    @Input public double musi_co1;

    @Role(PARAMETER)
    @Input public double musi_co2;

    @Role(PARAMETER)
    @Input public double musi_co3;
    @Role(PARAMETER)
    @Input public double musi_co4;

    @Description("HRU")
    @Input public HRU hru;

    @Description("")
    @Input public double precip;

    @Description("curren crop")
    @Input public Crop crop;

    @Description("landuse")
    @Input public Landuse landuse;

    @Description("soil")
    @Input public SoilType soil;

    @Description("Current time")
    @Input public LocalDate time;

    @Description("HRU statevar RD1")
    @Input public double outRD1;

    @Description("snow depth")
    @Input public double snowDepth;

    @Description("surface temperature")
    @Input public double surfacetemp;

    @Input public double BioAct;

    @Description("HRU statevar sediment inflow")
    @Input @Output public double insed;

    @Description("HRU statevar sediment outflow")
    @Optional
    @Input @Output public double sedpool;

    @Description("HRU statevar sediment outflow")
    @Output public double outsed;

    @Description("soil loss")
    @Units("t/d")
    @Output public double gensed;

    @Override
    protected void run(Context context) {
        Musle component = new Musle();

        component.tempRes = tempRes;
        component.musi_co1 = musi_co1;
        component.musi_co2 = musi_co2;
        component.musi_co3 = musi_co3;
        component.musi_co4 = musi_co4;
        component.area = hru.area;
        component.precip = precip;
        component.slope = hru.slope;
        component.cFactor = (crop == null ? landuse.cFactor : crop.cfactor);
        component.rockFragment = soil.rockFragment;
        component.slopelength = hru.slopelength;
        component.kFactor = soil.kFactor;
        component.time = time;
        component.outRD1 = outRD1;
        component.snowDepth = snowDepth;
        component.surfacetemp = surfacetemp;
        component.BioAct = BioAct;
        component.landuseID = hru.landuseID;
        component.insed = insed;
        component.sedpool = sedpool;

        component.execute();

        insed = component.insed;
        sedpool = component.sedpool;
        outsed = component.outsed;
        gensed = component.gensed;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
